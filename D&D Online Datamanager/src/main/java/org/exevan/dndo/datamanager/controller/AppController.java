package org.exevan.dndo.datamanager.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.net.URL;

@FXMLController
public class AppController {

    @Autowired
    private ApplicationContext appContext;

    @FXML
    private AnchorPane root;

    private Logger logger = LoggerFactory.getLogger(AppController.class);

    public void onNewCharacterSheetBtnClick(final Event event) {
        logger.info("AppController#onNewCharacterSheetBtnClick");

        switchScene("");
        closeWindow();
    }

    public void onNewCreatureBlockBtnClick(final Event event) {
        logger.info("AppController#onNewCreatureBlockBtnClick");

        switchScene("newCreatureSheet/NewCreatureSheetView");
        closeWindow();
    }

    public void onNewItemBlockBtnClick(final Event event) {
        logger.info("AppController#onNewItemBlockBtnClick");

        switchScene("");
        closeWindow();
    }

    public void onNewSpellBlockBtnClick(final Event event) {
        logger.info("AppController#onNewSpellBlockBtnClick");

        switchScene("");
        closeWindow();
    }

    public void onExitBtnClick(final Event event) {
        logger.info("AppController#onExitBtnClick");

        closeApp();
        closeWindow();
    }

    private void closeApp() {
        SpringApplication.exit(appContext, new ExitCodeGenerator() {
            @Override
            public int getExitCode() {
                return 0;
            }
        });
    }

    private void switchScene(String name) {

        String path = "/fxml/" + name + ".fxml";

        try {
            URL url = getClass().getResource(path);
            Parent root = new FXMLLoader(url).load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeWindow() {
        Stage stage = (Stage) root.getScene().getWindow();
        stage.close();
    }

}
