package org.exevan.dndo.datamanager.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import org.exevan.dndo.sheets.domain.general.Alignment;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureSize;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreatureSheetBaseDataController {

    private Logger logger = LoggerFactory.getLogger(CreatureSheetBaseDataController.class);

    @FXML
    private ChoiceBox<CreatureSize> sizeBox;

    @FXML
    private ChoiceBox<CreatureType> typeBox;

    @FXML
    private ChoiceBox<Alignment> alignmentBox;

    public CreatureSheetBaseDataController() {
        logger.info("CreatureSheetBaseDataController#new: " + this);
    }

    @FXML
    public void initialize() {
        sizeBox.getItems().setAll(CreatureSize.values());
        typeBox.getItems().setAll((CreatureType.values()));
        alignmentBox.getItems().setAll(Alignment.values());
    }

}
