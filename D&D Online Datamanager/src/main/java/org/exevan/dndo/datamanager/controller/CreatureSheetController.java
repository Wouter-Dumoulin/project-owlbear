package org.exevan.dndo.datamanager.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@FXMLController
public class CreatureSheetController {

    private Logger logger = LoggerFactory.getLogger(CreatureSheetController.class);

    @FXML
    private CreatureSheetBaseDataController newCreatureSheetBaseDataFormController;

    @FXML
    private Parent newCreatureSheetIntro;

    @FXML
    private Parent newCreatureSheetBaseDataForm;

    @FXML
    private Parent newCreatureSheetAttributeForm;

    private int step = 0;

    private List<Parent> sceneOrder = new ArrayList<Parent>();


    public CreatureSheetController() {
        logger.info("CreatureSheetController#new");
    }

    @FXML
    public void initialize() {
        System.out.println("bork");

        sceneOrder.add(newCreatureSheetIntro);
        sceneOrder.add(newCreatureSheetBaseDataForm);
        sceneOrder.add(newCreatureSheetAttributeForm);

        sceneOrder.get(step).setVisible(true);
    }

    public void onPrevBtnClick(final Event event) {
        logger.info("CreatureSheetController#onPrevBtnClick");

        sceneOrder.get(step).setVisible(false);

        step -= 1;

        sceneOrder.get(step).setVisible(true);

    }

    public void onNextBtnClick(final Event event) {
        logger.info("CreatureSheetController#onNextBtnClick");


        sceneOrder.get(step).setVisible(false);

        step += 1;

        sceneOrder.get(step).setVisible(true);
    }


    /**
    private RestTemplate template = new RestTemplate();

    public void getCreatureSheets() {
        //ResponseEntity<String> response = this.template.getForEntity(URI.create("http://localhost:8080/sheets/creatures/all"), String.class);
        //System.out.println(response.getBody());
        CreatureSheet[] sheets = this.template.getForObject(URI.create("http://localhost:8080/sheets/creatures/all"), CreatureSheet[].class);
        System.out.println(sheets.toString());
    }
     **/
}
