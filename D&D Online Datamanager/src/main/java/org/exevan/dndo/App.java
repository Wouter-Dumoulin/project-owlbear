package org.exevan.dndo;

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.exevan.dndo.datamanager.view.AppView;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launchApp(App.class, AppView.class, args);
    }
}
