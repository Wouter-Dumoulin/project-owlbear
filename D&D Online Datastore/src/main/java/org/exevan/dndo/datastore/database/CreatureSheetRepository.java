package org.exevan.dndo.datastore.database;

import org.exevan.dndo.sheets.domain.sheets.creature.CreatureSheet;
import org.springframework.data.repository.CrudRepository;

public interface CreatureSheetRepository extends CrudRepository<CreatureSheet, Long> {
}
