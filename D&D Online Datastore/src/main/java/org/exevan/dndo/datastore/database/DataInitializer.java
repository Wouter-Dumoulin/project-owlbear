package org.exevan.dndo.datastore.database;

import org.exevan.dndo.sheets.domain.general.Alignment;
import org.exevan.dndo.sheets.domain.general.Dice;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureData;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureSheet;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureSize;
import org.exevan.dndo.sheets.domain.sheets.creature.CreatureType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataInitializer implements ApplicationRunner {

	private static final Logger logger = LoggerFactory.getLogger(DataInitializer.class);

	private CreatureSheetRepository creatureSheetRepository;

	@Autowired
	public DataInitializer(CreatureSheetRepository creatureSheetRepository) {
		this.creatureSheetRepository = creatureSheetRepository;
	}

	@Override
	public void run(ApplicationArguments applicationArguments) throws Exception {
		logger.info("Seeding Database - Started");

		CreatureSheet[] creatureSheets = {

				new CreatureSheet(
						new CreatureData(
								"Efreet",
								CreatureSize.LARGE,
								CreatureType.ELEMENTAL,
								Alignment.LAWFUL_EVIL,
								17,
								Dice.D10,
								16
						)
				),

				new CreatureSheet(
						new CreatureData(
								"Werebear",
								CreatureSize.MEDIUM,
								CreatureType.HUMANOID,
								Alignment.NEUTRAL_GOOD,
								10,
								Dice.D8,
								18
						)
				),
		};

		this.creatureSheetRepository.save(Arrays.asList(creatureSheets));


		logger.info("Seeding Database - Finished");
	}
}
