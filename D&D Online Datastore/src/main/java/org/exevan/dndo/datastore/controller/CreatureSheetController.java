package org.exevan.dndo.datastore.controller;

import org.exevan.dndo.datastore.database.CreatureSheetRepository;


import org.exevan.dndo.sheets.domain.sheets.creature.CreatureSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreatureSheetController {

    private CreatureSheetRepository repository;

    @Autowired
    public CreatureSheetController(CreatureSheetRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/sheets/creatures/all")
    public Iterable<CreatureSheet> getAllCreatureSheets() {
        System.out.println("CreatureSheetController#getAllCreatureSheets");
        Iterable<CreatureSheet> sheets = this.repository.findAll();
        return sheets;
    }
}
