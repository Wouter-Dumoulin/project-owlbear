package org.exevan.dndo.sheets.domain.sheets.creature;

import javax.persistence.*;

@Entity
@Table(name = "CreatureSheets")
public class CreatureSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long Id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "CREATURE_ID")
    private CreatureData creatureData;

    public CreatureSheet() {
    }

    public CreatureSheet(CreatureData creatureData) {
        this.creatureData = creatureData;
    }


    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public CreatureData getCreatureData() {
        return creatureData;
    }

    public void setCreatureData(CreatureData creatureData) {
        this.creatureData = creatureData;
    }
}
