package org.exevan.dndo.sheets.domain.sheets.creature;

import org.exevan.dndo.sheets.domain.general.Alignment;
import org.exevan.dndo.sheets.domain.general.Dice;

import javax.persistence.*;

@Entity
@Table(name = "CreatureData")
public class CreatureData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CreatureName")
    private String name;

    @Column(name = "CreatureSize")
    @Enumerated(EnumType.STRING)
    private CreatureSize size;

    @Column(name = "CreatureType")
    @Enumerated(EnumType.STRING)
    private CreatureType type;

    @Enumerated(EnumType.STRING)
    private Alignment alignment;

    private int armorClass;

    @Enumerated(EnumType.STRING)
    private Dice hitDice;

    private int hitDiceAmount;

    public CreatureData() {
    }

    public CreatureData(String name, CreatureSize size, CreatureType type, Alignment alignment, int armorClass, Dice hitDice, int hitDiceAmount) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.alignment = alignment;
        this.armorClass = armorClass;
        this.hitDice = hitDice;
        this.hitDiceAmount = hitDiceAmount;
    }

    public CreatureData(long id, String name, CreatureSize size, CreatureType type, Alignment alignment, int armorClass, Dice hitDice, int hitDiceAmount) {
        this(name, size, type, alignment, armorClass, hitDice, hitDiceAmount);
        this.id = id;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreatureSize getSize() {
        return size;
    }

    public void setSize(CreatureSize size) {
        this.size = size;
    }

    public CreatureType getType() {
        return type;
    }

    public void setType(CreatureType type) {
        this.type = type;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public int getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(int armorClass) {
        this.armorClass = armorClass;
    }

    public Dice getHitDice() {
        return hitDice;
    }

    public void setHitDice(Dice hitDice) {
        this.hitDice = hitDice;
    }

    public int getHitDiceAmount() {
        return hitDiceAmount;
    }

    public void setHitDiceAmount(int hitDiceAmount) {
        this.hitDiceAmount = hitDiceAmount;
    }
}
