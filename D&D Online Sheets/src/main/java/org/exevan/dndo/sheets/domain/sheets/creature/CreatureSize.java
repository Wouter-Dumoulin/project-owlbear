package org.exevan.dndo.sheets.domain.sheets.creature;

public enum CreatureSize {
    TINY,
    SMALL,
    MEDIUM,
    LARGE,
    HUGE,
    GARGANTUAN
}
